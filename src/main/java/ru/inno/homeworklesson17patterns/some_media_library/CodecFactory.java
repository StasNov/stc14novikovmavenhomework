package ru.inno.homeworklesson17patterns.some_media_library;

public class CodecFactory {
    public static Codec extract(VideoFile file) {
        String type = file.getCodecType();
        if (type.equals("mp4")) {
            System.out.println("CodecFactory: extracting mpeg ...");
            return new MPEG4CompressionCodec();
        } else {
            System.out.println("CodecFactory: extracting mkv ...");
            return new MKVCompressionCodec();
        }
    }
}
