package ru.inno.homeworklesson17patterns;

import ru.inno.homeworklesson17patterns.facade.VideoConversionFacade;

import java.io.File;

public class DemoUseFacade {
    public static void main(String[] args) {
        VideoConversionFacade converter = new VideoConversionFacade();
        File mp4video = converter.convertVideo("newvideo.mkv", "mp4");


    }
}
