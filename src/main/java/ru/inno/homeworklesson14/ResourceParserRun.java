package ru.inno.homeworklesson14;

import java.util.concurrent.BlockingQueue;

/**
 * Поток для запуска нахождения вхождения слова массива words в тексте ресурса
 */
public class ResourceParserRun implements Runnable {
    private URLLoader url;
    private String[] words;
    private BlockingQueue<String> queue;

    /**
     * Конструктор
     *
     * @param url   url ресурса
     * @param words массив слов
     * @param queue очередь для добавления результата нахождения вхождения
     */
    public ResourceParserRun(URLLoader url, String[] words, BlockingQueue<String> queue) {
        this.url = url;
        this.words = words;
        this.queue = queue;
    }

    /**
     * Запуск нахождения вхождения слова массива words в тексте ресурса
     */
    @Override
    public void run() {
        ResourceParser resourceParser = new ResourceParser(url, words, queue);
        resourceParser.textReader();
    }
}
