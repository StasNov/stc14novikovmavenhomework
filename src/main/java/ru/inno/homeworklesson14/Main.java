package ru.inno.homeworklesson14;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        String[] wordForWarAndPeace = {"Petersburg"};
        String[] wordsForManyFiles = {"человек", "плата", "свет", "голоса", "больная", "скоро"};
        searcher("src/main/java/ru/inno/homeworklesson14/resources/warandpeace/",
                wordForWarAndPeace,
                "src/main/java/ru/inno/homeworklesson14/result/ResultWarEndPeace.txt",
                "Result for 'War and Peace' - ", 20);
        searcher("src/main/java/ru/inno/homeworklesson14/resources/middlecountoffiles/",
                wordsForManyFiles,
                "src/main/java/ru/inno/homeworklesson14/result/ResultAfterManyFiles.txt",
                "Result for many files - ", 20);
    }

    /**
     * Поиск вхождения слов (из массива words) в текстовые предложения, содержащиеся в файлах,
     * которые хранятся в папке folderName. Предложения, в которых найдены слова, записываются в файл,
     * путь к которому указан в res.
     *
     * @param folderName папка с файлами, содержащими предложения в которых осуществляется поиск слов массива words
     * @param words      массив слов
     * @param res        путь к файлу куда записываются предложения в которых было найдено хотя бы одно слово из
     *                   массива words
     * @param message    сообщение о завершении работы метода
     * @param pools      количество потоков
     */
    private static void searcher(String folderName, String[] words, String res, String message, int pools) throws IOException, InterruptedException {
        OutputWriter writer = new OutputWriter();
        ExecutorService executorService = Executors.newFixedThreadPool(pools);
        DataParserInterface parcer = new DataParserInterfaceImpl(executorService, writer);
        FilesInFolderSearcher fileSearcher = new FilesInFolderSearcher(new File(folderName));
        Searcher searcher = new Searcher(fileSearcher, parcer);
        searcher.search(words, res, message);
    }
}
