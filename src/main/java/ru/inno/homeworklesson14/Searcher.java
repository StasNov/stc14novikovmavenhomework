package ru.inno.homeworklesson14;

import java.io.IOException;

/**
 * Поисковик вхождения слов в текстовые предложения, содержащиеся в файлах.
 */
public class Searcher {
    private DataParserInterface parser;
    private FilesInFolderSearcher filesSearcher;

    /**
     * Конструктор
     *
     * @param filesSearcher поисковик файлов в папке
     * @param parser        Интерфейс парсинга данных
     */
    public Searcher(FilesInFolderSearcher filesSearcher, DataParserInterface parser) {
        this.filesSearcher = filesSearcher;
        this.parser = parser;
    }

    /**
     * Поиск вхождения слов (из массива words) в текстовые предложения, содержащиеся в файлах,
     * пути к которым предоставляет filesSearcher, переданный в конструктор. Предложения, в которых найдены слова,
     * записываются в файл, путь к которому указан в res.
     *
     * @param words   массив слов
     * @param res     путь к файлу куда записываются предложения в которых было найдено хотя бы одно слово из
     *                массива words
     * @param message сообщение о завершении работы метода
     */
    public void search(String[] words, String res, String message) throws IOException, InterruptedException {
        String[] files = filesSearcher.getFilesFromFolder();
        long beginTine = System.currentTimeMillis();
        parser.getOccurences(files, words, res);
        long endTime = System.currentTimeMillis();
        double result = (endTime - beginTine) / 1000.;
        System.out.println(message + result + " sec.");
    }

}
