package ru.inno.homeworklesson14;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Поисковик файлов в папке
 */
public class FilesInFolderSearcher {

    private File folder;

    /**
     * Конструктор
     *
     * @param folder экземпляр класса File с переданным в конструктор параметром path,
     *               где path - путь к папке с файлами.
     */
    public FilesInFolderSearcher(File folder) {
        this.folder = folder;
    }

    /**
     * Получение списка путей к файлам, хранящимся в папке
     *
     * @return массив строк, хранящий список путей к файлам в указанной папке
     */
    public String[] getFilesFromFolder() {
        File[] listOfFiles = folder.listFiles();
        List<String> fileNames = new ArrayList<>();
        Stream.of(listOfFiles).forEach(value -> fileNames.add(String.valueOf(value)));
        return fileNames.toArray(new String[fileNames.size()]);
    }


}
