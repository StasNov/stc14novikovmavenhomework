package ru.inno.homeworklesson14;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;


/**
 * Поток вывода предложений, удовлетворяющих условиям поиска, в файл
 */
public class OutputWriter extends Thread {
    private String path;
    private BlockingQueue<String> queue;

    /**
     * Получение BlockingQueue<String>
     *
     * @return BlockingQueue<String> queue
     */
    public BlockingQueue<String> getQueue() {
        return queue;
    }

    /**
     * Setter для queue
     *
     * @param queue очередь BlockingQueue<String>
     */
    public void setQueue(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    /**
     * Setter для path
     *
     * @param path путь к файлу в который будет осуществляться запись
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Запись содержимого очереди в файл до тех пор, пока извне не будет вызван interrupt() для OutputWriter
     */
    @Override
    public void run() {
        try (FileOutputStream fileOutputStream = new FileOutputStream(path)) {
            while (true) {
                fileOutputStream.write(queue.take().getBytes());
            }
        } catch (InterruptedException | IOException e) {
            e.getMessage();
        }
    }
}
