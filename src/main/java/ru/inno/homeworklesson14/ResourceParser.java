package ru.inno.homeworklesson14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;

/**
 * Парсер одного ресурса
 */
public class ResourceParser {
    private URLLoader url;
    private String[] words;
    private BlockingQueue<String> queue;

    /**
     * Конструктор
     *
     * @param url   url ресурса
     * @param words массив слов
     * @param queue очередь для добавления результата нахождения вхождения
     */
    public ResourceParser(URLLoader url, String[] words, BlockingQueue<String> queue) {
        this.url = url;
        this.words = words;
        this.queue = queue;
    }

    /**
     * Считывание предложений с одного ресурса
     */
    public void textReader() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.getInputStream()))) {
            StringBuilder buffer = new StringBuilder();
            int data;
            while ((data = reader.read()) != -1) {
                char symbol = (char) data;
                if (symbol == '.' || symbol == '!' || symbol == '?') {
                    buffer.append(symbol + "\n");
                    finder(buffer.toString());
                    buffer = new StringBuilder();
                } else {
                    buffer.append(symbol);
                }
            }
        } catch (IOException | InterruptedException e) {
            System.err.print("IOException");
        }
    }

    /**
     * Поиск вхождения слов массива words в предложение
     *
     * @param sentence строка предложения
     */
    private void finder(String sentence) throws InterruptedException {
        for (String word : words) {
            if (sentence.toUpperCase().contains(word.toUpperCase())) {
                queue.put(sentence);
                break;
            }
        }
    }

}
