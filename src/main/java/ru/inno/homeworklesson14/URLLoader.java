package ru.inno.homeworklesson14;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class URLLoader {
    private URL url;

    /**
     * Конструктор
     *
     * @param path адрес ресурса
     */
    public URLLoader(String path) throws IOException {
        url = toURL(path);
    }

    /**
     * Проверка является ли адрес ресурса путем к удаленному ресурсу или файлу
     *
     * @param path адрес
     * @return URL к ресурсу
     */
    private URL toURL(String path) throws IOException {
        URL url;
        try {
            url = new URL(path);
        } catch (MalformedURLException e) {
            url = new File(path).toURI().toURL();
        }
        return url;
    }

    /**
     * Открытие потока ввода
     *
     * @return InputStream созданного в конструкторе url
     */
    public InputStream getInputStream() throws IOException {
        return url.openStream();
    }

    /**
     * Получение url
     *
     * @return url
     */
    public URL getUrl() {
        return url;
    }
}
