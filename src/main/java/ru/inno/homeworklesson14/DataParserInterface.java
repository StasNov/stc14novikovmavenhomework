package ru.inno.homeworklesson14;

import java.io.IOException;

/**
 * Интерфейс парсинга данных
 */
public interface DataParserInterface {

    /**
     * Поиск вхождения слов (из массива слов) в текстовые предложения, содержащиеся в файлах ресурсов.
     * Запись предложений (в которых найдены слова) в файл, путь к которому указан в res.
     *
     * @param sources массив текстовых файлов
     * @param words   массив слов
     * @param res     путь к файлу, в который будут записываться предложения
     */
    void getOccurences(String[] sources, String[] words, String res) throws InterruptedException, IOException;
}
