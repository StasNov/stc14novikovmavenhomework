package ru.inno.homeworklesson14;

import java.io.IOException;
import java.util.concurrent.*;

public class DataParserInterfaceImpl implements DataParserInterface {
    private ExecutorService pool;
    private OutputWriter writer;
    private BlockingQueue<String> queue;

    /**
     * Конструктор
     *
     * @param pool   пул потоков
     * @param writer поток вывода
     */
    public DataParserInterfaceImpl(ExecutorService pool, OutputWriter writer) {
        this.pool = pool;
        this.writer = writer;
        queue = new LinkedBlockingDeque<>();
    }

    /**
     * Получение BlockingQueue<String>
     *
     * @return BlockingQueue<String> queue
     */
    public BlockingQueue<String> getQueue() {
        return queue;
    }

    @Override
    public void getOccurences(String[] sources, String[] words, String res) throws InterruptedException, IOException {
        writer.setPath(res);
        writer.setQueue(queue);
        Thread fileWriter = writer;
        fileWriter.start();
        for (String source : sources) {
            URLLoader url = new URLLoader(source);
            pool.execute(new ResourceParserRun(url, words, queue));
        }
        pool.shutdown();
        pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        fileWriter.interrupt();
    }
}
