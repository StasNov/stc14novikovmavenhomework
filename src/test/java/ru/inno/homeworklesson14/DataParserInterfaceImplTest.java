package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class DataParserInterfaceImplTest {
    private DataParserInterfaceImpl dataParser;
    private ExecutorService executorService;
    private OutputWriter writer;
    private ResourceParserRun resourceParserRun;


    @BeforeEach
    void setUp() {
        executorService = Mockito.mock(ExecutorService.class);
        writer = Mockito.mock(OutputWriter.class);
        resourceParserRun = Mockito.mock(ResourceParserRun.class);
        dataParser = new DataParserInterfaceImpl(executorService, writer);
    }

    /**
     * Сравнение ожидаемого размера очереди с размером очереди получившимся после выполения метода getOccurences().
     */
    @Test
    void getOccurences() throws InterruptedException, IOException {
        String[] words = new String[]{"test", "free"};
        String[] sources = {"hi", "hello"};
        String res = "Nothing";
        Mockito.doNothing().when(executorService).execute(resourceParserRun);
        Mockito.doNothing().when(executorService).shutdown();
        dataParser.getOccurences(sources, words, res);
        assertEquals(0, dataParser.getQueue().size());
    }

}