package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class FilesInFolderSearcherTest {
    String path;
    File folder;
    FilesInFolderSearcher filesInFolderSearcher;

    @BeforeEach
    void setUp() {
        path = "src/main/java/ru/inno/homeworklesson14/resources/warandpeace/";
        folder = new File(path);
        filesInFolderSearcher = new FilesInFolderSearcher(folder);
    }

    /**
     * Сравнение размера массива файлов, содержащихся в указанной папке, с размером массива путей к этим же файлам,
     * получившегося после выполения метода getFilesFromFolder().
     */
    @Test
    void getFilesFromFolder() {
        assertEquals(folder.listFiles().length, filesInFolderSearcher.getFilesFromFolder().length);
    }
}