package ru.inno.homeworklesson14;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class URLLoaderTest {

    /**
     * Проверка на возникновение FileNotFoundException
     */
    @Test
    void getInputStreamForText() throws IOException {
        String path = "file";
        URLLoader urlLoader = new URLLoader(path);
        assertThrows(FileNotFoundException.class, () -> urlLoader.getInputStream());
    }

    /**
     * Проверка преобразования пути к файлу в url
     */
    @Test
    void getUrlFromText() throws IOException {
        String path = "file";
        URLLoader urlLoader = new URLLoader(path);
        assertEquals(new File(path).toURI().toURL(), urlLoader.getUrl());
    }

    /**
     * Проверка преобразования строки, содержащей путь к интернет-ресурсу, в url
     */
    @Test
    void getUrlFromURL() throws IOException {
        String path = "https://yandex.ru/";
        URLLoader urlLoader = new URLLoader(path);
        assertEquals(new URL(path), urlLoader.getUrl());
    }
}