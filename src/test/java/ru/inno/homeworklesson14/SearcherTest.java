package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class SearcherTest {
    private Searcher searcher;
    private DataParserInterface dataParserInterface;
    private FilesInFolderSearcher fileSearcher;

    @BeforeEach
    void setUp() {
        dataParserInterface = Mockito.mock(DataParserInterface.class);
        fileSearcher = Mockito.mock(FilesInFolderSearcher.class);
        searcher = new Searcher(fileSearcher, dataParserInterface);
    }

    /**
     * В случае успешного завершения работы метода search(words, res, message) в System.out записывается message
     * и время работы метода.
     * Проверка наличия строки message в System.out.
     */
    @Test
    void search() throws IOException, InterruptedException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));
        String[] words = new String[]{"test", "free"};
        String[] sources = {"hi", "hello"};
        String res = "Nothing";
        String message = "Result";
        String[] files = {"xxxx"};
        Mockito.when(fileSearcher.getFilesFromFolder()).thenReturn(files);
        Mockito.doNothing().when(dataParserInterface).getOccurences(sources, words, res);
        searcher.search(words, res, message);
        assertTrue(outContent.toString().contains(message));
        System.setOut(originalOut);
    }
}