package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import static org.junit.jupiter.api.Assertions.*;

class ResourceParserTest {
    private String[] words;
    private String text;
    private BlockingQueue<String> queue;
    private URLLoader url;
    private ResourceParser parser;

    @BeforeEach
    void setUp() {
        url = Mockito.mock(URLLoader.class);
        words = new String[]{"test", "free"};
        text = "This is a test sentence. I want to break free!";
        queue = new LinkedBlockingDeque<>();
        parser = new ResourceParser(url, words, queue);
    }

    /**
     * Сравнение ожидаемого размера очереди с размером очереди получившимся после выполения метода textReader().
     */
    @Test
    void textReader() throws IOException {
        Mockito.when(url.getInputStream()).thenReturn(getInputStream(text));
        parser.textReader();
        assertEquals(2, queue.size());
    }

    /**
     * В случае возникновения ошибки при выполнении метода textReader() в System.err записывается строка "IOException".
     * Сравнение строки "IOException" с содержимым System.err.
     */
    @Test
    void textReaderException() throws IOException {
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        PrintStream originalErr = System.err;
        System.setErr(new PrintStream(errContent));
        Mockito.when(url.getInputStream()).thenThrow(new IOException());
        parser.textReader();
        assertEquals("IOException", errContent.toString());
        System.setErr(originalErr);
    }

    /**
     * Получение InputStream от строки
     *
     * @param text строка
     * @return ByteArrayInputStream от переданной строки
     */
    private InputStream getInputStream(String text) {
        return new ByteArrayInputStream(text.getBytes());
    }

}