package ru.inno.homeworklesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;

class OutputWriterTest {
    private OutputWriter outputWriter;
    private String path;
    private BlockingQueue<String> queue;

    @BeforeEach
    void setUp() {
        outputWriter = new OutputWriter();
        path = "src/main/java/ru/inno/homeworklesson14/result/ResultJUnit.txt";
        queue = new LinkedBlockingQueue<>();
    }

    /**
     * Установление через set-метод изначально пустой очереди.
     * Сравнение ожидаемого размера очереди с размером очереди получившимся после выполения метода run().
     * Подтверждение того, что состояние очереди не изменилось.
     */
    @Test
    void run() {
        outputWriter.setPath(path);
        outputWriter.setQueue(queue);
        outputWriter.start();
        outputWriter.interrupt();
        assertEquals(0, outputWriter.getQueue().size());
    }

}